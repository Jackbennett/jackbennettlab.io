const webpack = require("webpack")

module.exports = {
  title: "jackben.net",
  description: "new site in development",
  dest: "public",
  head: [
    ["link", { rel: "icon", type: "image/svg+xml", href: "/favicon.svg" }],
    ["link", { rel: "alternate icon", href: "/favicon.ico" }],
  ],
  markdown: {
    lineNumbers: false,
    extendMarkdown: (md) => {
      md.use(require("markdown-it-html5-embed"), { html5embed: {} })
    },
  },
  themeConfig: {
    nav: [
      { text: "Home", link: "/" },
      { text: "Projects", link: "/blog.html" },
    ],
    sidebar: "auto",
    head: [
      [
        "link",
        {
          rel: "stylesheet",
          href: "https://fonts.googleapis.com/css?family=B612:700&display=swap",
        },
      ],
    ],
  },
  plugins: [
    [
      "@vuepress/google-analytics",
      {
        ga: "UA-173075283-1", // UA-00000000-0
      },
    ],
  ],
  configureWebpack: (config, isServer) => {
    if (!isServer) {
      return {
        plugins: [new webpack.EnvironmentPlugin(["SHOW_DRAFTS"])],
      }
    }
  },
}
