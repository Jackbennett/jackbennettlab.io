const Vuex = require("vuex")

const ADDHINT = "ADDHINT"
const REMOVEHINT = "REMOVEHINT"

const ADDSELECT = "ADDSELECT"
const REMOVESELECT = "REMOVESELECT"

module.exports = function({ Vue, options }) {
  Vue.use(Vuex)
  const store = new Vuex.Store({
    state: {
      hint: new Set(),
      select: new Set(),
    },
    mutations: {
      [ADDHINT](state, name) {
        state.hint.add(name)
      },
      [REMOVEHINT](state, name) {
        state.hint.delete(name)
      },
      [ADDSELECT](state, name) {
        state.select.add(name)
      },
      [REMOVESELECT](state, name) {
        state.select.delete(name)
      },
    },
  })

  Object.assign(options, { store })

  Vue.mixin({
    computed: {
      $sortedBlogPages() {
        let showDraft = process.env.SHOW_DRAFTS || false
        return this.$site.pages
          .filter(({ path }) => path.indexOf("/blog/") >= 0)
          .filter(
            ({ frontmatter }) => showDraft || frontmatter["draft"] !== true
          ) // not equal so empty keys also appear
          .sort((a, b) => {
            return (
              new Date(b.frontmatter.authorDate).getTime() -
              new Date(a.frontmatter.authorDate).getTime()
            )
          })
      },
    },
  })
}
