// what am I (themeEntry): https://v1.vuepress.vuejs.org/theme/option-api.html#theme-configuration

const dateFromString = require("./use/dateFromString.js")
const enhanceApp = require("./enhanceApp.js")

module.exports = (themeConfig, ctx) => {
  return {
    extendPageData($page) {
      const {
        _filePath, // file's absolute path
        _computed, // access the client global computed mixins at build time, e.g _computed.$localePath.
        _content, // file's raw content string
        _strippedContent, // file's content string without frontmatter
        key, // page's unique hash key
        frontmatter, // page's frontmatter object
        regularPath, // current page's default link (follow the file hierarchy)
        path, // current page's real link (use regularPath when permalink does not exist)
      } = $page
      frontmatter.authorDate = dateFromString(path)
    },
    plugins: [enhanceApp],
  }
}
