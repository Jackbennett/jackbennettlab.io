module.exports = function dateFromString(str) {
  // /blog/2020-07-01-gpo-design.html
  // match year-[month]-[day]
  const pathDatePattern = /(?<year>\d{4})-?(?<month>\d{1,2})?-?(?<day>\d{1,2})?-/
  let strTimes = str.match(pathDatePattern)?.groups
  let date = []
  for (let period in strTimes) {
    switch (period) {
      case "day":
        date[2] = parseInt(strTimes[period])
        break
      case "month":
        date[1] = parseInt(strTimes[period]) - 1
        break
      case "year":
        date[0] = parseInt(strTimes[period])
        break
      default:
        throw "string was not a pattern of year-month-day"
    }
  }
  return new Date(Date.UTC(...date))
}
