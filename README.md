---
title: About
layout: Split
sidebar: auto
---

I'm an IT Pro &amp; Developer with 10+ years experience, below is some of those areas

[Personal Gitlab](https://gitlab.com/Jackbennett) | [Github](https://github.com/JackBennett)

## Stack

Computing is a layer of skills. I've labelled my experience in those layers you can expand below;

<Stackitem id="Design">

- Web Design
- PCB Schematic and layout
- Education lesson and Training Resources
- Project Planning and Costing
- Helpdesk and Documentation

</Stackitem>

<Stackitem id="Development">

- Javascript, Vue, D3, Ember, Backbone,
- Node.js
- Powershell
- Python
- SQL, Postgres
- Redis
- CouchDB
- Terraform
- Git version control and collaboration

</Stackitem>

<Stackitem id="Application">

- Nginx and Apache Webserver Configuration
- Google G-Suite Setup and Administration
- Microsoft Active Directory
- Microsoft Applocker policy deployment
- Microsoft Deployment Tools Desktop Image Creation & Deployment
- Microsoft Group Policy
- Microsoft User Experience Virtualization Configuration
- KiCAD Electronic Design Automation
- Papercut Print Audit and Policy Design
- SIMS.net School Information System

</Stackitem>

<Stackitem id="Operations">

- Windows 2000 -> Server 2019
- Hyper-V
- VMware, vSpere, vCenter
- Docker Container Development and Compose Deployment
- ChromeOS
- Linux - Fedora, CoreOS, Ubuntu, Hypriot, Arch, Alpine, Debian, RancherOS, ESXi (I realise some of these are related)
- Gitlab CI/CD build and release pipelines
- Kubernetes
- Microsoft Failover Clustering
- Digital Ocean DNS and VPS administration
- Heroku

</Stackitem>

<Stackitem id="Network">

- Ipv4 Address and VLAN Design and Management
- DNS Public and Internal Management
- DHCP with Active Directory Failover
- Wireguard

</Stackitem>

<Stackitem id="Hardware">

- Desktop specification and assembly/installation
- Server specification and installation
- Arduino
- Raspberry Pi Node.js GPIO
- [Espurino](https://www.espruino.com/Pico) prototyping board and JS language subset.
- Basic Electronics

</Stackitem>

## Experience

<Timeline />

## In the Future

Some future areas of interest I would like to work in are;

- Hardware - ESP (8266 and 32) wireless microcontroller boards and rust
- Network - IPv6 Design and implementation
- Network - ACL segregation along with IPv6 deployment
- Operations - GCP or other cloud platforms more than ssh into a VPS
- Operations - k3s from rancher for IoT-like projects looks interesting
- [Design - PCB produced to order](blog/2019-11-11-LED-PCB/), a real product from designs. Diagrams have so far been simple enough by hand and only for documentation purposes.
