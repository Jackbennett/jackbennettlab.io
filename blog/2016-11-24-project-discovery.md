---
title: Projcet Discovery To Display Student Progress
sidebar: false
stack:
  - application
  - development
---

["Project Discovery"](https://github.com/Jackbennett/project-discovery)(Github) was my idea to provide a tiny web server for each student to run themselves in class were they learned to write HTML.

## What

This would build upon prior lessons on using the command line, by applying it for a use students can see. All they need to do is navigate to the correct folder and run the command to start the server, basic but important skills.

## Why

The thought was teachers would run their own class page at the start of the lesson, as students started their own work they'd be added to the display at the front of the class. As servers come on/offline while being worked on the status is reflected on the screen.

Aside from being a fun thing to look at, this intuitively covers the client/server model and give them some controls to play with should they poke around further.

## How

As a network admin I get to set options in the environment for this to work well like ensuring software dependencies are available and OS environment options (project server path) to connect by default, all in the name of making lessons run smoother.

I regret to state while initial use in my club was promising further uptake by teaching staff has been lackluster.
