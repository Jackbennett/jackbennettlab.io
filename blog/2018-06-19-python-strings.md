---
ttile: Python Strings In Detail
sidebar: false
stack:
  - design
  - development
---

## Learning Python One Tiny Piece At A Time

After supporting GCSE students learning python, I've summarised areas of programming/python that more than one person hasn't been aware of which really slows them down. I don't think anything that follows is complex, rather if a person skips over some of this without building an understanding then later on things get really hard. Written from basic to advanced.

If you like this, I have self guided examples you can get from [my gitlab](https://gitlab.com/Jackbennett/learn-python) that cover a bunch of areas of python in a progressive manner, building as you go.

### How To Teach Yourself

```python
test = 'example of a string'
print(test)
# output: example of a string
```

Everyone knows print, it's lesson 0, students often don't leap to using it temporarily for inspecting what isn't working, they are reluctant to edit and chop up code while they tourble shoot it, instead building more and more of the program entirely inside their head. I think this is because you're never show what programming looks like, you just get to see finished code and work on tutorals.

There's more than simply print if you spend a minute looking at the python docs. `type(test)` would tell you it's a string, `dir(test)` then shows you the methods a string has that you can use. `repr(test)` would be an accurate representation of the variable, perhaps not in a pretty format for humans, super handy for lists and dictionaries.

When trying to solve a problem, A great way to learn is first assigning your variables, then using `dir()` against them. Go look up any useful sounding words from the output in pythons documentation. You might have a method that does exactly what you need. Or even to check the spelling or names of methods you know should be there. Is it push or append to add to a List?

Now what about when things stop working.

```python
print(1 + "1")
# output: An error "unsupported operand type(s) for +: 'int' and 'str'"
```

That being, the plus (+) operand in python doesn't know what to do with a number and a string. You might as well be adding 1 + "potato" for all python sees. Just like maths, 1 + 1 forwards or backwards is the same thing right?... right?

```python
print("1" + 1)
# output: Tricky error as "Can't convert 'int' object to str implicitly"
```

So what does implicit mean? "not explicit; implied; indirect" or rather. When told to be quite, the threat of detention is implied.
Computer science is full of terms you won't normally come across, but don't be put off it's generaly the first thing you'd guess. We all look it up anyway. The error is telling you to convert the type `int` to a `str` yourself e.g. `print("1" + str(1))`

You can think of print as a function, lets make our own so we don't need to worry about this;

```python
def customPrint(*string):
    words = [] # Function will give a new list of strings back, so lets make that list
    for word in string: # Loop over everything we're given calling each one "word"
        words.append(str(word)) # Add each thing to the list we made but first call str on it to force the type.
    return " ".join(words) # Give back a new list of strings joined by the string of whitespace

customPrint("1 +", 1, "= 2")
# output: 1 + 1 = 2
```

**_Personal Note_**

When I write python, it irks me a bit compared to other languages with its outside-in style. It's very "Thing I want in the end" -> "perform it on what I have" style. Look at line 5 above (`return...`) _using a space, joing the array words_. I think much more like `words.join(' ')`, _join the words array with a space_. Probably my JS background.

This isn't really improtant now but as a final thing on strings. When you use `str()` on something, that thing actually has a special double underscored method that's called with transforms the data inside into a string to present. Python lets you override this;

```python
class myString(str):
    def __str__(s):
        return "example"

testCustomString = myString('Here is a sentence')
repr(testCustomString) # repr is a function to get the actual representation. note this also has the quotes to that denote a string.

print(type(testCustomString))
print(dir(testCustomString))
```

## Lists

After strings list's aren't much more complicated, you've acutally already been using them. A list simply has an index for the values within it.

```python
list_Of_Numbers = [1, 2, 3, 4, 5]
```

Should be thought of as;

| **index:** | 0        | 1        | 2        | 3        | 4        |
| ---------- | -------- | -------- | -------- | -------- | -------- |
| **value:** | `<int>1` | `<int>2` | `<int>3` | `<int>4` | `<int>5` |

```python
list_Of_Numbers = [1, 2, 3, 4, 5]
print(list_Of_Numbers[3]) # print the third index value
# output: 4
```

Strings are a list with letters inside. That's why `"Example"[0]` returns `E`.

Objects, or rather Dictionaries as python calls them would be the next thing you should study. In short, think of it as a list but the index isn't a number automatically made, it's also a value you chose to set. This seems like a very under-taught type. In fact most students I've had to help don't know it at all, instead using many layers of a multi-dimensional array. Great excuse for a good word but way, way not easier.

```python
list_Of_Numbers = {'five': 5, 'four':4, 'three':3, 'one':1, 'two':2}
print(list_Of_Numbers['one']) # print the "one" index value
# output: 1
```
