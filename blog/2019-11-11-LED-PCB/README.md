---
title: LED PCB from scratch to control a robot
sidebar: false
quality: 700
stack:
  - design
  - hardware
  - development
---

All my [Hardware Club Code](https://gitlab.com/BirkdaleHigh/hardware-club-starter/tree/master) is on Gitlab.

<div><video controls="controls" preload="metadata"><source type="video/webm" src="./flashing-cropped.webm">
Your browser does not support playing HTML5 video. You can <a href="./flashing-cropped.webm" download="">download a copy of the video file</a> instead.
Here is a description of the content: Finished Board
</video></div>

I've been working on a pet project for a while to use hardware to teach kids an intro to programming.
As part of that I found the opportunity to have a professional PCB of my own made from start-to-finish.
The toy robot we found couldn't be more perfect, depending on what light sensor is on the motors do something.
I made a PCB that's the right size to lock into the clamp in the robot with lights in the right place, plus debugging LEDs in red to validate what your program is doing.

Remember the students in year 7 haven't done programming or electricity never mind circuits and in 4 lesson we get through;

1. Wire a simple LEDs circuit
1. Program a sequence of LEDs, using function abstractions & parameters for the adept.
1. Use those to drive a robot around
1. using PWM to control LED brightness and motor speed

The [Robot Controller](https://gitlab.com/BirkdaleHigh/hardware-club-starter/tree/master/RobotController) section of it has a bunch of info, prototypes and the [kiCad files](https://gitlab.com/BirkdaleHigh/hardware-club-starter/tree/master/RobotController/PCB) if anyone wants to make their own. I documented the batch I made inside manufacture.log.

This robot is perfect because it's basically an elaborate optocoupler for the micro controller and motors without having to explain that at this point.

After this we get onto the "I" part of I/O with the serial port, buttons and analog values for sensors like temperature and distance. There's examples in the other folders in the hardware club project. The goal is to cover enough ground to let students drive their own projects and learning.
