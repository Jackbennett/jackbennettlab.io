---
title: Learn Python
sidebar: false
draft: true
quality: 500
stack:
  - design
---

Evidently I'm writing a series on in learning Python. Part of my job is supporting teachers with lesson material for delivering and learning Python themselves. It made sense to start writing how I've learned as a tutorial for others.

[lean-python is on my gitlab](https://gitlab.com/Jackbennett/learn-python) is the root source for all my other posts on Python.
