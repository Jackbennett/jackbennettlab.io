---
title: Group Policy and AD Purpose Based Design
sidebar: false
draft: false
quality: 900
stack:
  - design
---

This advice has been created from years of fixing and improving a few existing sites domains as opposed to the point of view from a consultant that needs to blast out a good working set and move on. Primarily, GPO is a super old design, isn't getting any love from Microsoft and should be avoided if at all possible. It's very difficult to introduce modern codes of practice upon, but I believe below lists the best you can do with a bad tool.

## Guiding Principals

### 1. Purpose Based GPO Logic
Use a single GPO that applies everything you need to accomplish your goal but only that goal. Do not mix multiple purposes for a GPO so that they can be linked to composed and link to multiple locations later on.

For example to provide a consistent user experience my redirected folders, desktop icons, profile paths, custom start menu and locations, chrome user paths are are all in `Set-Personal Profile `

However you'll find a split policies that `Restrict-ControlPanel-Default visible items` and in a child OU's `Add-ControlPanel-Language Options` for certain users. Speaking of naming policies;

### 2. Verb-Noun *Like* Naming Scheme as Verb-Purpose
One idea I'd like to move over from powershell is the Verb-Noun pattern naming functions. It's self documented for anyone to simply run `powershell.exe -noexit -command get-verb` and pick what they like. (You might want to sort that list with `powershell.exe -noexit -command get-verb | sort-object verb`)

To keep the structure consistent. e.g. set-*, new-*, add-*, remove-*... at a minimum. I will admit, I use "fix-*" that's not in that list for quick policies that must fix and issue right now that I might not fully understand. We'll come back to that.

Overall, consistency is king. So stick with whatever make sense for you.

If you're lucky enough to be in a situation to dictated convention with the people editing GPO you might like to go further. I add an optional key to group related polices with a key like `<Verb>-[<Key[]>-]<Purpose Description>` e.g. `Set-WindowsUpdate-Generic rules` a  parent of `Set-WindowsUpdate-High Risk Clients Install immidiatly`, `Set-WindowsUpdate-Server Install times`, `Set-WindowsUpdate-Group-Server`

Without spaces between `-` it makes skipping to policy names easier to type in a long list. You can use that character in a file name should you write separate documentation elsewhere.

### 3. Avoid undo settings from above, prefer to overwrite to update them.
If required then restructure policies or AD until setting specificity goes down the tree. If you set a true to a false, it's default was false anyway, then you would be better served getting that policy setting applied to only things that need to be true and leaving the default otherwise.

On the other hand, you might need a `Set-Generic Update Time` policy in the parent tree to catch everything and another `Set-High Risk Client Update Time` perhaps just targeted at kiosks which overwrites the time. I think that's fine because see point 4 about repetition.

### 4. Avoid repetition
You should only need to create a setting once, and link that setting policy to multiple locations.
If you split rules to avoid changing a parent setting, you should avoid repeating identical values.

In the above `Update Time` policy role example, setting the update source location key would make sense in the generic higher level policy. Do not repeat the source location key if it's not changed in the lower down  `high risk clients` policy.

i.e. if you've built a website, treat GPO like you should be treating CSS, always try to be less specific in rules.

### 5. Avoid Enforced and Disabled inheritance
What these do is smash any organizing you might have and will make it exponentially more difficult to edit settings in the future as you use those options more. If you refactor the structure you should never need them anyway. In addition I'd avoid using the loopback processing modes which again flattens the structure, there are valid use cases to use Replace mode so I wouldn't call it always bad.

In my opinion they have one use case, and that's in a testing OU tree to replicate some other structure in the AD and test changes in there.

### 6. Lean on filtering
Use security filtering where appropriate to add more specificity to policies if restructuring the OU would be too drastic. Use security groups, then item-level targeting and finally WMI filters which I find the least clear to look back on 6 months later. If you whole team is happy looking at wmi filters then they are great.

You need to keep `domain computers` allowed to read the policy for the user on them to figure out if it even applies or not. "read" and "apply" are two separate permissions in advanced. Always "read" for authenticated users (which includes domain computers) and GPO modelling will keep working, if you don't you get an error that wmi can't resolve the policy. You can edit the domain to do this by default but you need to know what you're doing.

### 7. Refactor
So if you apply the guiding principals, look down your OU tree and see a number of `remove-*`, `undo-*` or especially `fix-*` it might be time to refactor by combining settings to make a generic policy up the tree or split up some settings to move down and reuse in multiple OU's.

### 8. Document! Use the description fields
As GPO lacks any auditing and change control by default, give yourself a break and leverage the setting description fields and even the policy overall description. Write what you intend to do, link supporting documentation or explain counter-intuitive settings. GPO is full of enable-to-disable feature settings. Especially registry keys! Don't forget you can make a setting group of keys and just document the group instead of each key.

What you set now might change in a update or work around an issue that gets solved by something else in 2 years time. You want to know what might happen if you take it out.

Later on you could use powershell or another tool to dump all the GPO titles, descriptions, setting names and their descriptions for a nice report on how the network is implemented. Pretty useful for onboarding people.

### final rule, break the rules
All rules have exceptions, you may know why you want to ignore them. As long as that's documented in an appropriate place I think you're good.

## Words of caution

You can't always move OU's around willy-nilly but they're not entire untouchable either. It's analogous to a file system but you shouldn't treat them like one. There might be hard coded applications that need certain objects located at the OU path they are at (Google Directory Sync being one). Generally, 2 or 3 levels below a the root OU's you won't find rules being more specific and you're free to re-arrange as required.

This Purpose based GPO breaks down on a small number of certain settings that don't aggregate but overwrite so watch out for those. Chrome's allowed plugins lists does not append to a parent setting, it would get replaced.

Keep secrets out of the policies, anything in the organisation can pull those fields and read it. That rule applied to anything in AD by the way.

Test. Test. Test. Since you're working with smaller single purpose GPO's it's easier to test. You can have a test OU right at the end of an OU path with a subset of client to try a new setting, gradually move that linked policy up the tree as you're confidant of it's impact. At that point you can quickly "disable link" if there's a problem, delete link to start again or refactor the setting into a parent policy once your happy.


## Conclusion
If you were to follow the above, I think you'd see why the advice from Microsoft is that your AD structure shouldn't follow your organisation structure, but rather the functional structure followed by your clients (yes users AND computers). You'll organically make changes to that match how your organisation operates and

## Why

Lets understand how GPO works to reason about the above conclusions

Settings are applied in the order `Local -> Site -> Domain -> OU -> Enforced`, in addition that order repeats for `Computer` and `User` settings, so be mindful of what settings you actually want to apply to users vs computers and where the policies are linked.

## Myths
Long loading times don't come from GPO number until like 10,000 GPO's or high latency network links. Settings all get aggregated into their respective processing engine in to apply, the winning setting is then determined and finally they're processes in the order the GPO runs the engines. You can do things which make this slow but that's not related to GPO number.

###### Notes and Sources
1. Policy scoping from "understanding group policy order" [4sysops.com](https://4sysops.com/archives/understanding-group-policy-order/)
2. GPO logon impact, read all parts of "how group policy impacts logon performance" [helgeklein.com](https://helgeklein.com/blog/2015/11/how-group-policy-impacts-logon-performance-1-cses/)
