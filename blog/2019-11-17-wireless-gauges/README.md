---
title: Wireless Gauges from Aircraft
sidebar: false
quality: 700
stack:
  - hardware
  - development
---

£20 from Ebay got me these sweet gauges, it's about 300 degree range 0-12 x10 RPM. But actually that's just 0-100% range with and extra 20% (Handy to re-implement the space shuttle engine range.)

I got two expecting one to be destroyed taking it apart figuring out how I'm going to run this from an esp8266, they are super easy to pull apart and really well made. Turns out aircraft grade parts are really nice, who knew.

<img src="./gauge-bits.jpg" alt="Gauge Components">

The plan is to couple a VID29 stepper motor (the kind your tacho or speed gauge is made with) to something on the wifi or BLE to have a remote 0-100% readout of whatever i want. Why not simply use the parts already inside it, well you'll see from the photos how I think this works and it's not exactly maker friendly. I say "think" because I struggled to find any information about this online, it might even use 400Hz AC I've no idea.

The dial is very lightly spring loaded to return to zero, and that is coupled by a magnet on the face of the motor. I think the motor is synchronised from the actual engines and the spinning magnet applies a certain force that's calibrated from the spring which results in the dial reading.

<div>
<video controls="controls" preload="metadata"><source type="video/webm" src="./make-it-spin.webm">
Your browser does not support playing HTML5 video. You can <a href="./make-it-spin.webm" download="">download a copy of the video file</a> instead.
Here is a description of the content: Spinning Gauge
</video>
</div>

It looks like I'm lucky in that the range of the stepper matches the dials range, so all I have to do is fabricate something to match the servo shaft and clockwork drum. There's loads of space in the housing to fit everything.

The hard work was from [guy.carpenter.id.au](https://guy.carpenter.id.au/gaugette/resources) making the Arduino library, I'm not sure if I'll use arduino when I'm finished but this has proved I can get everything working on the bench.

Next I'll laser cut some kind of coupler, and if it's wireless I'm going to have to get an antenna outside of the metal case.

Finally, if anyone was wondering the writing on the back of the gauges is;

```
INDICATOR ELECTRONIC TACHOMETER
type no. A32437 10.021, Serial no.274
Indicates 100% at 4200RPM Gen Speed
Kollsman instrument corporation, Elmhurst, New York.
```
