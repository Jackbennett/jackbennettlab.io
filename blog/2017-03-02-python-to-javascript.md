---
title: Python to Javascript
sidebar: auto
quality: 10
stack:
  - design
---

This is a lesson resource to kickstart students in a javascript project whose previously only seen python. It's a reference not a guide.

## Syntax Reference

Syntax is the word used to define the rules a language uses to define itself.
For example some English syntax rules would be;

1. All sentences start with a capital letter.
1. All sentences end with a period (full stop).
1. A period`.` denotes a rest when spoken.

Often there are multiple ways to get similar behavior between the
languages. You will find the first option most similar between the languages
first in their table cell.

<table>
  <tr>
    <th>Name</th>
    <th>Python</th>
    <th>Javascript</th>
  </tr>
  <tr>
    <td>Comment</td>
    <td><pre class="language-python"># A single line comment</pre></td>
    <td><pre class="language-javascript">// A JS single line comment</pre></td>
  </tr>
  <tr>
    <td>Variable assignment</td>
    <td><pre class="language-python">name = 1</pre></td>
    <td><pre class="language-javascript">var name = 1</pre></td>
  </tr>
  <tr>
    <td>Boolean</td>
    <td><pre class="language-python">True<br />False</pre></td>
    <td><pre class="language-javascript">true<br />false</pre></td>
  </tr>
  <tr>
    <td>Print to screen</td>
    <td><pre class="language-python">print('Example something')</pre></td>
    <td><pre class="language-javascript">console.log('Example Something')</pre></td>
  </tr>
  <tr>
    <td>Statement separator</td>
    <td colspan="2"><pre class="language-python">newline<br />;</pre></td>
  </tr>
  <tr>
    <td>String literal</td>
    <td>
      <pre class="language-python">&quot;hello world&quot;<br />'hello world'<br />&quot;&quot;&quot;hello
      world&quot;&quot;&quot;<br />'''hello world'''</pre>
    </td>
    <td>
      <pre class="language-javascript">&quot;hello world&quot;<br />'hello world'</pre>
    </td>
  </tr>
  <tr>
    <td>Array literal</td>
    <td>
      <pre class="language-python">[1, 2, 'etc']<br />(1, 2, 'etc')<br />{1,
      2,'etc'}</pre>
    </td>
    <td><pre class="language-javascript">[1, 2, 'etc']</pre></td>
  </tr>
  <tr>
    <td>Object literal</td>
    <td colspan="2">
      <pre class="language-python">{&quot;key&quot;: &quot;string value&quot;, &quot;number&quot;: 1}</pre>
    </td>
  </tr>
  <tr>
    <td>String index</td>
    <td colspan="2"><pre class="language-python">&quot;Some Words&quot;[0]</pre></td>
  </tr>
  <tr>
    <td>Array index</td>
    <td colspan="2">
      <pre class="language-python">[1,2,3, &quot;four&quot;][0]</pre>
    </td>
  </tr>
  <tr>
    <td>Object property</td>
    <td>
      <pre class="language-python">myObjct = {&quot;name&quot;: &quot;demo&quot;}<br />myObjct[&quot;name&quot;]</pre>
    </td>
    <td>
      <pre class="language-javascript">var myObjct = {&quot;name&quot;: &quot;demo&quot;}<br />myObjct[&quot;name&quot;]<br />o.name</pre>
    </td>
  </tr>
  <tr>
    <td>Conditionals</td>
    <td>
<pre class="language-python">
test = True
if test == True:
  print(&quot;correct&quot;)
elif test == "never":
  print('never going to run')
else:
  print('Test is false')</pre>
    </td>
    <td>
<pre class="language-javascript">var test = true
if(test == true){
  console.log(&quot;correct&quot;)
}
else if (test == "never"){
  console.log("never going to run")
} else {
  console.log("Test is false")
}</pre>
    </td>
  </tr>
  <tr>
    <td>Function definition</td>
    <td>
      <pre class="language-python">def funcName():<br />&nbsp;&nbsp;# Code inside the function must be
      indented</pre>
    </td>
    <td>
      <pre class="language-javascript">function funcName(){<br />//Code must be inside code blocks closing bracket<br />}</pre>
    </td>
  </tr>
  <tr>
    <td>Language methods</td>
    <td>all lower case, usually short e.g. <code>&quot;hi&quot;.upper()</code></td>
    <td>camelCase, more wordy e.g. <code>&quot;hi&quot;.toUpperCase()</code></td>
  </tr>
</table>

## Code examples

This example is to show how similar yet different javascript is from python

```python
def shout(word):
  return str(word).upper()
```

```javascript
function shout(word) {
  return word.toString().toUpperCase()
}
```

Note how Javascript uses more syntax to create the statement, it only happens
to look similar because it can be written that way. The following is exactly
the same function. Look at the brackets locations and white-space.

<pre class="language-javascript">function shout(word) {return word.toUpperCase()}</pre>
<pre class="language-javascript">function shout(word) {



return word.toUpperCase()}</pre>

All these examples produce this result when called

```javascript
shout('hi there') // HI THERE
```

## Glossary

Memorizing these words isn't important to programming, what is important
is recognizing the things that they do. These words become important when you
find yourself having to explain your code or thoughts to another human.

### Index

Like a book, there's an index field used to quickly reference specific
parts, often numbered by page, sometimes a section number. In data structures
like Objects and Arrays that hold multiple fields of information you might
find the index useful to access.

<table>
  <tr>
    <th>code</th>
    <td colspan="5"><code>['a','b','c','d','e']</code></td>
  </tr>
  <tr>
    <th>index</th>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
  </tr>
  <tr>
    <th>value</th>
    <td>a</td>
    <td>b</td>
    <td>c</td>
    <td>d</td>
    <td>e</td>
  </tr>
</table>

If you want to access an index by name instead of number, you now understand
the difference between and array and an object.

### Method, definition, function

A block of code that can be re-used, if it's given a name to be reused by
we'd start to call it a method. Generally any of those names are fine to
use.

### Literal

You don't always have to put something in a named variable to use it,
especially if it's not going to be reused. Take the following identical
code as an example;

```javascript
var line = 'Example sentence.' // Declare the variable named &quot;line&quot; and assign it the value of a string literal
console.log(line) // Print out the value of the variable line, which would be a string, assigned earlier
```

```javascript
console.log('Example sentence.') // Print the literal string of example sentence.
```
