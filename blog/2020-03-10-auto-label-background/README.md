---
title: Automatic PC Name In Logon Background
sidebar: false
draft: false
quality: 500
stack:
  - development
  - operations
---

![Classroom preview](./classroom_background.png)

This is one of those unexpected quality-of-life improvements from just wanting to do something fun.

Powershell uses `$PSComputerName` for inserting text onto an image. I didn't realise how useful this was going to be until I saw teachers directing students to sit at specific PC's marshalling them in from the door, it meant our seating plans suddenly were uniform across any room and exams department could have a much easier time planning for room seating.

## Make a background-ready image

I made a wrapper to just add text to a given image. That's here `Add-Watermark` [on github](https://github.com/Jackbennett/powershell-1/blob/master/Add-Watermark.ps1), it just needs and in-file, out and the text you desire. You can also specify the RGB and opacity of the text, as well as use any text. I named all the PC's at deployment with `PC<room><number>` when we deploy with MDT and they're all clockwise around the room. So from this the default parameter knows how to extract its own computer number from that pattern, this will probably require modifying for your organisation.

You'll note a greyscale switch, that was a later addition for some PC's I would call this twice. Once for the watermarked background and a greyscale version, the logon screen was grey and the background is colour so the user got a nice colourised image once at in.

## Setting the background

Now, this one is actually quite an old script. Windows 10 now has a new mechanism to customise these [as of 1709](https://docs.microsoft.com/en-us/windows/client-management/mdm/personalization-csp)

But for posterity [this is the version we ran for years](https://github.com/Jackbennett/powershell-1/blob/d5e82eff0983cd64ba5a5cbcb60ed1e30e4ed29e/Set-ComputerBackground.ps1) and that was called by a startup script on each client. More useful to you now might be the later additions of `Enable-PersonalizationCSP` and `New-CustomBackground` in [the latest version](https://github.com/Jackbennett/powershell-1/blob/c80d68c7ece945bffb69ee1a91ffbecc0a7629bc/Set-ComputerBackground.ps1). This is only roughed out, honestly the original never broke so I didn't push the newer one beyond my technician PC. Now I'd be tempted to set `DesktopImageUrl` and `LockScreenImageUrl` to my own https service an generate images on the fly if I can get enough client identity from the request.

## Side Effects

Since it was so easy to watermark any image, we started to have the school vote on student artwork to feature as the next background. One of which is pictured above.
